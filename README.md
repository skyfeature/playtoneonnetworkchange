# README #

How to add repository to git/Bitbucket:
Step1: Create repo on bitbucket
Step2: Go to inside the project.
Step3: Open terminal and type the following commands in order:
	1. git init
	2. git remote add origin https://skyfeature@bitbucket.org/skyfeature/playtoneonnetworkchange.git (I got this from bitbucket by clicking on "I already have repository")
	3. git add .
	4. git push -u origin master
Step4: Done.